package gigichien.apitest.listener;

public abstract class APIListener {
    abstract public void onAPISuccess();

    abstract public void onAPIError(String msg);

    public void onNetworkError() {
        onAPIError("連線錯誤");
    };

    public void onHTTPError(int statusCode) {
        onAPIError("伺服器錯誤");
    };
}
