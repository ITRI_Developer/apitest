package gigichien.apitest.listener;

public interface APIRequestTaskListener {
    //TODO: too more callback needed implement?
    public void onAPIComplete(String result);

    public void onNetworkError();

    public void onHttpError(int statusCode);
};