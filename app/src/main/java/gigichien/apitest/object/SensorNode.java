package gigichien.apitest.object;

import java.util.ArrayList;

public class SensorNode {
    public String ID = "";

    public SensorNode(String id) {
        ID = id;
    };
    public ArrayList<SensorNodeRecord> nodeRecords = new ArrayList<SensorNodeRecord>();

    public void addNodeRecord(SensorNodeRecord record) {
        nodeRecords.add(record);

    }
    public void resetNodeRecords() {
        nodeRecords.clear();
    }

    public ArrayList<SensorNodeRecord> getNodeRecords() {
        return nodeRecords;
    }
}
