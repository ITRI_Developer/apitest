package gigichien.apitest.object;

import java.util.ArrayList;

public class LPN {
    public String ID = "";
    private ArrayList<SensorNode> sensorNodeList = new ArrayList<SensorNode>();
    private ArrayList<Container> containerList = new ArrayList<Container>();

    public LPN(String id) { ID = id; };

    public void addNode(SensorNode sensorNode) {
        if (!sensorNodeList.contains(sensorNode)) {
            sensorNodeList.add(sensorNode);
        }
    }

    public void deleteNode(SensorNode sensorNode) {
        sensorNodeList.remove(sensorNode);
    }

    public void resetNodeList() {
        sensorNodeList.clear();
    }

    public void setContainerList(ArrayList<Container> containerList) {
        this.containerList.clear();
        this.containerList.addAll(containerList);
    }

    public ArrayList<SensorNode> getSensorNodeList() {
        return sensorNodeList;
    }

    public ArrayList<Container> getContainerList() {
        return containerList;
    }
}
