package gigichien.apitest.object;

import java.util.ArrayList;

public class Container {
    public String ID = "";
    public SensorNode sensorNode;
    private ArrayList<LocationRecord> locationRecords = new ArrayList<LocationRecord>();

    public ArrayList<LocationRecord> getLocationRecords() {
        return locationRecords;
    }

    public void addLocationRecord(LocationRecord record) {
        locationRecords.add(record);

    }

    public void resetLocationRecords() {
        locationRecords.clear();
    }
}
