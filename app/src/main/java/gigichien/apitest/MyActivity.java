package gigichien.apitest;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import gigichien.apitest.api.lpn.DeleteLPNListAPI;
import gigichien.apitest.api.lpn.GetLPNListAPI;
import gigichien.apitest.api.login.LoginAPI;
import gigichien.apitest.api.lpn.SetLPNListAPI;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.LPN;
import gigichien.apitest.object.SensorNode;


public class MyActivity extends Activity {

    // API
    LoginAPI loginAPI;
    SetLPNListAPI setLPNListAPI;
    GetLPNListAPI getLPNListAPI;
    DeleteLPNListAPI deleteLPNListAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        initAPIComponent();
        initButtonListeners();
    }

    private void initAPIComponent() {
        loginAPI = new LoginAPI(this, new APIListener() {
            @Override
            public void onAPISuccess() {
                Toast.makeText(MyActivity.this, "Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAPIError(String msg) {
                Toast.makeText(MyActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });

        getLPNListAPI = new GetLPNListAPI(this, new APIListener() {
            @Override
            public void onAPISuccess() {
                Toast.makeText(MyActivity.this, "Success", Toast.LENGTH_SHORT).show();
                Log.i(ConstUtil.TAG, "getLPN : " + getLPNListAPI.getLPNList().toString());
            }

            @Override
            public void onAPIError(String msg) {
                Log.i(ConstUtil.TAG, "onAPIError = " + msg);
                Toast.makeText(MyActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });

        setLPNListAPI = new SetLPNListAPI(this, new APIListener() {
            @Override
            public void onAPISuccess() {

            }

            @Override
            public void onAPIError(String msg) {

            }
        });

        deleteLPNListAPI = new DeleteLPNListAPI(this, new APIListener() {
            @Override
            public void onAPISuccess() {

            }

            @Override
            public void onAPIError(String msg) {

            }
        });
    }


    private void initButtonListeners() {
        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginAPI.resetParams();
                loginAPI.start();
            }
        });
        findViewById(R.id.btn_set_lpn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LPN inputLPN = new LPN("Android_06");
                inputLPN.addNode(new SensorNode("2750963650016543290"));
                setLPNListAPI.setInputData(inputLPN);
                setLPNListAPI.resetParams();
                setLPNListAPI.start();
            }
        });
        findViewById(R.id.btn_download_lpn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLPNListAPI.resetParams();
                getLPNListAPI.start();
            }
        });
        findViewById(R.id.btn_del_lpn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteLPNListAPI.setInputData(new LPN("Android_05"));
                deleteLPNListAPI.resetParams();
                deleteLPNListAPI.start();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
