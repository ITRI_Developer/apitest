package gigichien.apitest.api;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import gigichien.apitest.ConstUtil;
import gigichien.apitest.listener.APIRequestTaskListener;

public class APIRequestTask extends AsyncTask<Void, Void, Void> {
    private final static String TAG_PREFIX = "APIRequestTask ";
    private final String url;
    private boolean isNetworkError = false;
    private boolean isHttpStatusError = false;
    private int httpStatusCode;
    private String getParams = "";
    private ArrayList<NameValuePair> postParams;

    protected APIRequestTaskListener apiRequestTaskListener;
    protected String httpResult;


    public APIRequestTask(String url, APIRequestTaskListener listener) {
        this.url = url;
        apiRequestTaskListener = listener;
    }

    public void setGetParam(String getParams) {
        this.getParams = getParams;
    }

    public void setPostParam(ArrayList<NameValuePair> postParams) {
        if (this.postParams == null) {
            this.postParams = new ArrayList<NameValuePair>();
        } else {
            this.postParams.clear();
        }
        this.postParams.addAll(postParams);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isNetworkError = false;
        isHttpStatusError = false;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        sendRequest();
        return null;
    }

    private void sendRequest() {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse;
            if (postParams != null) {
                HttpPost postMethod = new HttpPost(url);
                postMethod.setEntity(new UrlEncodedFormEntity(postParams, "utf-8"));
                httpResponse = httpClient.execute(postMethod);
            } else if (getParams != null) {
                HttpGet getMethod = new HttpGet(url + getParams);
                httpResponse = httpClient.execute(getMethod);
            } else {
                //TODO:Error type?
                Log.v(ConstUtil.TAG, TAG_PREFIX + "unknown request type");
                httpResult = "";
                return;
            }

            httpStatusCode = httpResponse.getStatusLine().getStatusCode();
            Log.v(ConstUtil.TAG, TAG_PREFIX + httpResponse.getStatusLine().getStatusCode() + ": " + url);
            if (httpStatusCode == 200) {
                httpResult = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            } else {
                isHttpStatusError = true;
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    protected void onPostExecute(Void aVoid) {
        if (isNetworkError) {
            apiRequestTaskListener.onNetworkError();
        } else if (isHttpStatusError) {
            apiRequestTaskListener.onHttpError(httpStatusCode);
        } else {
            apiRequestTaskListener.onAPIComplete(httpResult);
        }
        super.onPostExecute(aVoid);
    }

    public void stop() {
        this.cancel(true);
        if (apiRequestTaskListener != null) {
            apiRequestTaskListener = null;
        }
    }
}