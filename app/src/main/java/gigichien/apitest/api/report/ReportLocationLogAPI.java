package gigichien.apitest.api.report;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import gigichien.apitest.listener.APIListener;
import gigichien.apitest.api.APIBase;
import gigichien.apitest.object.Container;
import gigichien.apitest.object.LocationRecord;

public class ReportLocationLogAPI extends APIBase {
    private final static String TAG_PREFIX = "ReportLocationLogAPI";
    private final static String apiURL = "/api/reportLocationLog";

    private ArrayList<Container> containers;

    public ReportLocationLogAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public void setInputData(ArrayList<Container> containers) {
        this.containers.clear();
        this.containers.addAll(containers);
    }

    @Override
    protected void setInputParams() {
        addCommonPostParam();
        StringBuilder records = new StringBuilder();
        for (Container container : containers) {
            for (LocationRecord record : container.getLocationRecords()) {
                records.append(container.sensorNode.ID + "," + record.timestamp + "," + record.latitude + "," + record.longitude + "\n");
            }
        }
        records.deleteCharAt(records.length() - 1);
        addPostParam("location_log", records.toString());
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {

    }
}
