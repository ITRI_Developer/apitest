package gigichien.apitest.api.report;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.Container;
import gigichien.apitest.object.LocationRecord;
import gigichien.apitest.object.SensorNodeRecord;

public class ReportStatusAPI extends APIBase {
    private final static String TAG_PREFIX = "ReportStatusAPI";
    private final static String apiURL = "/api/reportStatus";
    private LocationRecord locationRecord;
    private ArrayList<Container> containers;

    public ReportStatusAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public void setInputData(LocationRecord locationRecord, ArrayList<Container> containers) {
        this.locationRecord = locationRecord;
        this.containers.clear();
        this.containers.addAll(containers);
    }

    @Override
    protected void setInputParams() {
        addCommonPostParam();
        //TODO:Define object
        addPostParam("latitude", locationRecord.latitude);
        addPostParam("longitude", locationRecord.longitude);
        addPostParam("timestamp", locationRecord.timestamp);
        StringBuilder records = new StringBuilder();
        for (Container container : containers) {
            for (SensorNodeRecord record : container.sensorNode.getNodeRecords()) {
                records.append(container.ID + "," + record.timestamp + "," + record.temperature + "," + record.power + "\n");
            }
        }
        addPostParam("temp_list", records.toString());
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {

    }
}
