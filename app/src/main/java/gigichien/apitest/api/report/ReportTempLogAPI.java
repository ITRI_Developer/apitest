package gigichien.apitest.api.report;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.Container;
import gigichien.apitest.object.SensorNodeRecord;

public class ReportTempLogAPI extends APIBase {
    private final static String TAG_PREFIX = "ReportTempLogAPI";
    private final static String apiURL = "/api/reportTempLogAPI";
    private ArrayList<Container> containers;

    public ReportTempLogAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public void setInputData(ArrayList<Container> containers) {
        this.containers.clear();
        this.containers.addAll(containers);
    }

    @Override
    protected void setInputParams() {
        addCommonPostParam();
        StringBuilder records = new StringBuilder();
        for (Container container : containers) {
            for (SensorNodeRecord record : container.sensorNode.getNodeRecords()) {
                records.append(container.ID + "," + record.timestamp + "," + record.temperature + "," + record.power + "\n");
            }
        }
        addPostParam("temp_log", records.toString());
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {

    }
}
