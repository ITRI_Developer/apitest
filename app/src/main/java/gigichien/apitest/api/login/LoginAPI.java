package gigichien.apitest.api.login;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import gigichien.apitest.ConstUtil;
import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;

public class LoginAPI extends APIBase {
    private final static String TAG_PREFIX = "LoginAPI";
    private final static String apiURL = "/login";

    public LoginAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
        needLogin = false;
    }

    @Override
    protected void setInputParams() {
        addPostParam("imei", deviceID);
        addPostParam("username", "epl");
        addPostParam("password", "epl");
        addPostParam("role", ConstUtil.USER_ROLE_GROUP.PICKER);
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {
        Log.i(ConstUtil.TAG, "resCode = " + jsonObject.toString());
        sessionID = jsonObject.optString("value");
    }
}
