package gigichien.apitest.api.lpn;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import gigichien.apitest.ConstUtil;
import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.LPN;
import gigichien.apitest.object.SensorNode;

public class GetLPNListAPI extends APIBase {
    private final static String TAG_PREFIX = "GetLPNListAPI";
    private final static String apiURL = "/api/getLPNList";
    private ArrayList<LPN> LPNList = new ArrayList<LPN>();

    public GetLPNListAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    @Override
    protected void setInputParams() {
        addCommonGetParam();
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {
        Log.i(ConstUtil.TAG, "resCode = " + jsonObject.toString());
        LPNList.clear();
        JSONObject value = jsonObject.optJSONObject("value");
        if (value != null) {
            JSONArray lpnList = value.optJSONArray("lpnList");
            if (lpnList != null && lpnList.length() > 0) {
                for (int i = 0; i < lpnList.length(); i++) {
                    LPNList.add(i, new LPN(lpnList.optJSONObject(i).optString("lpn")));
                    String[] tagList = lpnList.optJSONObject(i).optString("tag_list").split(",");
                    for (String tag : tagList) {
                        LPNList.get(i).addNode(new SensorNode(tag));
                    }
                }
            }
        }
    }

    public ArrayList<LPN> getLPNList() {
        for(LPN lpn : LPNList){
            Log.v(ConstUtil.TAG,TAG_PREFIX+"LPN ID: "+lpn.ID);
            for(SensorNode sensorNode : lpn.getSensorNodeList()){
                Log.v(ConstUtil.TAG,TAG_PREFIX+"TAG ID: "+ sensorNode.ID);
            }
        }
        return LPNList;
    }
}
