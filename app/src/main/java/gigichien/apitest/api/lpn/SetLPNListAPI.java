package gigichien.apitest.api.lpn;

import android.content.Context;

import android.util.Log;
import org.json.JSONObject;

import gigichien.apitest.ConstUtil;
import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.LPN;
import gigichien.apitest.object.SensorNode;

public class SetLPNListAPI extends APIBase {
    private final static String TAG_PREFIX = "SetLPNListAPI";
    private final static String apiURL = "/api/setLPNList";
    private LPN lpnAdded;
    public SetLPNListAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public void setInputData(LPN lpnAdded){
        this.lpnAdded = lpnAdded;
    }

    @Override
    protected void setInputParams() {
        StringBuilder node_list = new StringBuilder();
        for(SensorNode node : lpnAdded.getSensorNodeList()){
            node_list.append(node.ID);
        }
        Log.v(ConstUtil.TAG, TAG_PREFIX + node_list.toString());
        addCommonPostParam();
        addPostParam("lpn", lpnAdded.ID);
        addPostParam("tag_list",node_list.toString());
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {

    }
}
