package gigichien.apitest.api.lpn;

import android.content.Context;

import org.json.JSONObject;

import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.LPN;

public class DeleteLPNListAPI extends APIBase {

    private final static String TAG_PREFIX = "DeleteLPNListAPI";
    private final static String apiURL = "/api/delLPNList";
    private LPN lpnDel;
    public DeleteLPNListAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public void setInputData(LPN lpnDel){
        this.lpnDel = lpnDel;
    }

    @Override
    protected void setInputParams() {
        addCommonPostParam();
        addPostParam("lpn",lpnDel.ID);
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {

    }
}
