package gigichien.apitest.api;


import android.content.Context;
import android.telephony.TelephonyManager;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import gigichien.apitest.ConstUtil;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.listener.APIRequestTaskListener;

public abstract class APIBase {
    private final static String TAG_PREFIX = "APIBase ";
    private final String url;
    private String getParams = "";
    private ArrayList<NameValuePair> postParams;
    private APIRequestTask apiRequestTask;
    private APIListener apiListener;
    protected static String sessionID = null;
    protected static String deviceID;
    protected Context context;
    protected boolean needLogin = true;

    private APIRequestTaskListener apiRequestTaskListener = new APIRequestTaskListener() {
        @Override
        public void onAPIComplete(String result) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onAPIComplete :" + result);
            preParseResult(result);
        }

        @Override
        public void onNetworkError() {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onNetworkError");
            if (apiListener != null) {
                apiListener.onNetworkError();
            }
        }

        @Override
        public void onHttpError(int statusCode) {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "onHttpError");
            if (apiListener != null) {
                apiListener.onHTTPError(statusCode);
            }
        }
    };

    public APIBase(Context context, String apiURL, APIListener apiListener) {
        this.context = context;
        this.url = ConstUtil.API_URL_PREFIX + apiURL;
        this.apiListener = apiListener;
        init();
    }

    protected void init() {
        initDeviceId();
    }

    private void initDeviceId() {
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            String id = telephonyManager.getDeviceId();
            if (id != null) {
                deviceID = id;
                Log.v(ConstUtil.TAG, TAG_PREFIX + " device id : " + deviceID);
            } else {
                Log.v(ConstUtil.TAG, TAG_PREFIX + "initDeviceID error can't get");
            }
        } else {
            Log.v(ConstUtil.TAG, TAG_PREFIX + "initDeviceID error");
        }
    }

    protected void addCommonGetParam() {
        addGetParam("imei", deviceID);
        addGetParam("sid", sessionID);
    }

    protected void addCommonPostParam() {
        addPostParam("imei", deviceID);
        addPostParam("sid", sessionID);
    }

    public void resetParams() {
        if (postParams != null) {
            postParams.clear();
            postParams = null;
        }
        getParams = "";
    }

    protected void addGetParam(String key, String value) {
        if (getParams == "") {
            getParams = "?";
        } else {
            getParams += "&";
        }
        getParams += key + "=" + value;
    }

    protected void addPostParam(String key, String value) {
        if (postParams == null) {
            postParams = new ArrayList<NameValuePair>();
        }
        postParams.add((new BasicNameValuePair(key, value)));
    }

    public void start() {
        if (apiRequestTask != null) {
            apiRequestTask.cancel(true);
            apiRequestTask = null;
        }
        apiRequestTask = new APIRequestTask(url, apiRequestTaskListener);
        setInputParams();
        if (postParams != null) {
            apiRequestTask.setPostParam(postParams);
        } else {
            apiRequestTask.setGetParam(getParams);
        }
        apiRequestTask.execute();
    }

    protected abstract void setInputParams();

    private void preParseResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            int retCode = jsonObject.optInt("retCode");
            if (retCode == 0) {
                parseResult(jsonObject);
                if (apiListener != null) {
                    apiListener.onAPISuccess();
                }
            } else {
                if (apiListener != null) {
                    apiListener.onAPIError(jsonObject.optString("retMsg"));
                }
            }
        } catch (JSONException e) {
            Log.i(ConstUtil.TAG, "parse json failed");
            e.printStackTrace();
        }
    }

    ;

    protected abstract void parseResult(JSONObject jsonObject);

    public void stop() {
        if (apiRequestTask != null) {
            apiRequestTask.stop();
        }
    }
}
