package gigichien.apitest.api.shipping;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.LPN;

public class GetShippingOrderAPI extends APIBase {
    private final static String TAG_PREFIX = "GetShippingOrderAPI";
    private final static String apiURL = "/api/getShippingOrder";
    private LPN lpnShipping;
    private ArrayList<String> boxList = new ArrayList<String>();

    public GetShippingOrderAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public ArrayList<String> getBoxList() {
        return boxList;
    }

    public void setInputData(LPN lpnShipping) {
        this.lpnShipping = lpnShipping;
    }

    @Override
    protected void setInputParams() {
        addCommonPostParam();
        addPostParam("lpn", lpnShipping.ID);
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {
        String[] boxResult = jsonObject.optString("value").split("\n")[2].split(",");
        boxList.clear();
        for (String box : boxResult) {
            boxList.add(box.substring(1, box.length() - 2));
        }
    }
}
