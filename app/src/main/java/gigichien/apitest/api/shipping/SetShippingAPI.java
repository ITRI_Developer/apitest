package gigichien.apitest.api.shipping;

import android.content.Context;

import org.json.JSONObject;

import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.LPN;

public class SetShippingAPI extends APIBase {
    private final static String TAG_PREFIX = "SetShippingAPI";
    private final static String apiURL = "/api/setShipping";
    private LPN lpnShipping;
    private String shippingID;

    public SetShippingAPI(Context context, String apiURL, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public void setInputData(LPN lpnShipping, String shippingID) {
        this.lpnShipping = lpnShipping;
        this.shippingID = shippingID;
    }

    @Override
    protected void setInputParams() {
        addCommonPostParam();
        addPostParam("lpn", lpnShipping.ID);
        addPostParam("so_id", shippingID);
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {

    }
}
