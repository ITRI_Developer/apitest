package gigichien.apitest.api.shipping;

import android.content.Context;

import org.json.JSONObject;

import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;

public class SetDeliveredAPI extends APIBase {
    private final static String TAG_PREFIX = "SetDeliveredAPI";
    private final static String apiURL = "/api/setDelivered";
    private String shippingID;

    public SetDeliveredAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public void setInputData(String shippingID) {
        this.shippingID = shippingID;
    }

    @Override
    protected void setInputParams() {
        addCommonPostParam();
        addPostParam("so_id", shippingID);
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {

    }
}
