package gigichien.apitest.api.shipping;

import android.content.Context;

import org.json.JSONObject;

import gigichien.apitest.api.APIBase;
import gigichien.apitest.listener.APIListener;
import gigichien.apitest.object.LPN;

public class PutShippingOrderAPI extends APIBase {
    private final static String TAG_PREFIX = "PutShippingOrderAPI";
    private final static String apiURL = "/dev/putShippingOrder";
    private LPN lpnShipping;
    private String shippingID;
    public PutShippingOrderAPI(Context context, APIListener apiListener) {
        super(context, apiURL, apiListener);
    }

    public void setInputData(LPN lpnShipping, String shippingList) {
        this.lpnShipping = lpnShipping;
    }

    @Override
    protected void setInputParams() {
        addCommonPostParam();
        addPostParam("lpn", "Test01");
        addPostParam("username", "Android");

        StringBuilder boxList = new StringBuilder();
        for (int i = 0; i < lpnShipping.getContainerList().size(); i++) {
            if (i == 0) {
                boxList.append("{\n\"boxList\":\n[\"");
                boxList.append(lpnShipping.getContainerList().get(i));
                boxList.append("\"");
            } else if (i == lpnShipping.getContainerList().size() - 1) {
                boxList.append(",\"");
                boxList.append(lpnShipping.getContainerList().get(i));
                boxList.append("\"]\n}");
            } else {
                boxList.append(",\"");
                boxList.append(lpnShipping.getContainerList().get(i));
                boxList.append("\"");
            }
        }
        addPostParam("so", boxList.toString());
    }

    @Override
    protected void parseResult(JSONObject jsonObject) {
        shippingID = jsonObject.optString("value");
    }

    public String getShippingID(){
        return shippingID;
    }
}
