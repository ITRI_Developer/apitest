# README #

**1) Each API spec extends one webAPI
**

```
#!java

public class TestAPI extends APIBase {
    private final static String apiURL = "/api/Test";

    public TestAPI(Context context, APIListener apiListener) { super(context, apiURL, apiListener);}

   @Override
   protected void setInputParams() {
       addCommonPostParam();
       addPostParam();
       addPostParam();
        ...
      //Edit input parameters
    }

   @Override
   protected void parseResult(JSONObject jsonObject) {
        Log.v(TAG, jsonObject.toString());
   }
}

```

**2) Caller:**


```
#!java

TestAPI api = new TestAPI(context, apiURL, testAPIListener);

APIListener testAPIListener = new APIListener() {
    @Override
    public void onAPISuccess() {
         Log.v(TAG, "onAPISuccess");
         //Get web api result
     }

    @Override
    public void onAPIError(String msg) {
        Log.v(TAG, "onAPIError" + msg);
     }
  };
}

api.start(); //start API
```


 


**[ APIRequestTask ]**


1) APIRequestTaskListener

* onAPIComplete
* onNetworkError
* onHttpError

2) 繼承 AsyncTask，在 background thread 由 HttpClient 處理每一個 web API (POST/GET)

3) Caller 需代 APIRequestListener，並將結果經由註冊的 APIRequestListener 回傳 callback


**[ APIBase ]**


1) APIListener

*  onAPISuccess
*  onAPIError(String msg)
*  onNetworkError()
*  onHTTPError()

2) 所有的 API request 繼承於此 class

3) 封裝 APIRequestTask，由此執行 APIReqestTask

4) Caller 需代 APIListener，並將結果經由註冊的 APIListener 回傳 callback

5) API 說明如下:

* public APIBase(Context context, String apiURL, APIListener apiListener) : apiURL - web API URL

* protected boolean needLogin : 定義執行 web API 是否需判斷登入狀態
* protected void addCommonGetParam() : 加入 common GET Parameters (IMEI, SessionID)
* protected void addCommonPostParam() : 加入 common POST Parameters (IMEI, SessionID)
* protected void addGetParam(String key, String value) : 加入 GET parameters
* protected void addPostParam(String key, String value) : 加入 GET parameters
* protected abstract void setInputParams(); : 繼承之 class 需 override 此 function，在此執行 POST/GET 編輯動作
* protected abstract void parseResult(JSONObject jsonObject): 繼承之 class 需 override 此 function，在此執行 jsonObject result parsing

* public void start() : 執行 web API
* public void stop() : 停止 APIRequestTask
* public void resetParams() : 重設 POST/GET parameters